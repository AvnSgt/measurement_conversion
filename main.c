#include <stdio.h>
int main(void)
{
    /* 1 Kelpton equals 17.83 Reptoms */
    //Constant float used for conversion.
    const float REPTOM_CONVERSION_FACTOR = 17.83f; 
    
    // Standard greeting stored in character array.
    const char GENERAL_MESSAGE[] = {"Please enter number of " 
                                    "Keplton's measured: \n"};  
    
    // Units of kelptons that will be converted. Base is set to zero.
    float unitsofKelptons = 0.0f;  

    // A result variable to hold and output the calculation.
    float conversionResult = 0.0f; 

    // Output of string requst to user.  
    printf("%s", GENERAL_MESSAGE);

    //Retrive and store number of units from user.
    scanf("%f", &unitsofKelptons);

    // Conversion of units done here.
    conversionResult = unitsofKelptons * REPTOM_CONVERSION_FACTOR; 

    //Output results.
    printf("Keptons to Reptoms conversion is : %4f \n", conversionResult); 
    return 0;
}
